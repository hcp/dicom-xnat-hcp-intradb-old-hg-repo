/**
 * Copyright (c) 2012 Washington University
 */
package org.nrg.dcm;

import static org.nrg.dcm.SiemensPrivateAttributes.TAG_SIEMENS_IMAGE_INSTANCE_SHADOW_HEADER;
import static org.nrg.dcm.SiemensPrivateAttributes.TAG_SIEMENS_IMAGE_SERIES_SHADOW_HEADER;

import java.util.Collections;
import java.util.List;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.SiemensShadowHeader.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class SiemensShadowHeaderAttributeIndex implements DicomAttributeIndex {
    public static enum Type { INSTANCE, SERIES };
    private static final ImmutableMap<Type,Integer> tags = ImmutableMap.of(
            Type.INSTANCE, TAG_SIEMENS_IMAGE_INSTANCE_SHADOW_HEADER,
            Type.SERIES, TAG_SIEMENS_IMAGE_SERIES_SHADOW_HEADER);

    private final Logger logger = LoggerFactory.getLogger(SiemensShadowHeaderAttributeIndex.class);

    private final String field;
    private final Type type;
    private final int tag;

    public SiemensShadowHeaderAttributeIndex(final String field, final Type type) {
        this.field = field;
        this.type = type;
        this.tag = tags.get(type);
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getAttributeName(org.dcm4che2.data.DicomObject)
     */
    public String getAttributeName(final DicomObject o) {
        return "Siemens " + type + " Shadow Header field " + field;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getColumnName()
     */
    public String getColumnName() {
        return "Siemens"+type+"Shadow_"+field;
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getElement(org.dcm4che2.data.DicomObject)
     */
    public DicomElement getElement(final DicomObject o) {
        return o.get(tag);
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getPath(org.dcm4che2.data.DicomObject)
     */
    public Integer[] getPath(DicomObject o) {
        return new Integer[]{tag};
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getString(org.dcm4che2.data.DicomObject)
     */
    public String getString(DicomObject o) throws ConversionFailureException {
        final List<Field> fields = SiemensShadowHeader.getFields(o, getElement(o),
                Collections.singleton(Predicates.equalTo(field)));
        if (null == fields || fields.isEmpty()) {
            return null;
        } else {
            final Field f = Iterables.getFirst(fields, null);
            logger.trace("{} -> {}", getAttributeName(o), f.getValue());
            return null == f ? null : f.getValue();
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getString(org.dcm4che2.data.DicomObject, java.lang.String)
     */
    public String getString(DicomObject o, String defaultv) {
        try {
            final String v = getString(o);
            return null == v ? defaultv : v;
        } catch (ConversionFailureException e) {
            logger.info("shadow header element extraction failed", e);
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.DicomAttributeIndex#getStrings(org.dcm4che2.data.DicomObject)
     */
    public String[] getStrings(DicomObject o) {
        try {
            final String v = getString(o);
            return null == v ? null : v.split("\\");
        } catch (ConversionFailureException e) {
            logger.info("shadow header element extraction failed", e);
            return null;
        }
    }
}