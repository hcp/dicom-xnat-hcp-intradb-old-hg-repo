/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Set;

import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.io.DicomOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;

/**
 * @author Kevin A. Archie (karchie@wustl.edu)
 *
 */
public final class DicomStudyHttpPostSampler
extends MockDicomStudyAbstractSampler implements Sampler {
    public static final String SAMPLER_TITLE = "HTTP POST DICOM to XNAT";

    private static final long serialVersionUID = 1323134859515495593L;
    private static final String DEFAULT_TRANSFER_SYNTAX = TransferSyntax.ImplicitVRLittleEndian.uid();

    private final Logger logger = LoggerFactory.getLogger(DicomStudyHttpPostSampler.class);

    public DicomStudyHttpPostSampler() {
        super(SAMPLER_TITLE);
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.samplers.Sampler#sample(org.apache.jmeter.samplers.Entry)
     */
    public SampleResult sample(final Entry entry) {
        setAuthenticator();
        final URL importURL = getURL();
        final int[] counts = getCountsArray();

        final Set<String> uris = Sets.newLinkedHashSet();
        final SampleResult result = new SampleResult();
        result.setSampleLabel(SAMPLER_TITLE);
        result.setDataType(SampleResult.TEXT);
        result.setSamplerData(Arrays.toString(counts));

        result.sampleStart();
        try {
            final String studyInstanceUID = getNewUID();
            for (int series = 0; series < counts.length; series++) {
                final String seriesInstanceUID = getNewUID();
                for (int instance = 0; instance < counts[series]; instance++) {
                    final String sopInstanceUID = getNewUID();

                    final DicomObject o = makeTemplateObject(studyInstanceUID,
                            series, seriesInstanceUID,
                            instance, sopInstanceUID);
                    final String tsuid = o.getString(Tag.TransferSyntaxUID, DEFAULT_TRANSFER_SYNTAX);

                    final DicomObject fmi = new BasicDicomObject();
                    fmi.initFileMetaInformation(o.getString(Tag.SOPClassUID), sopInstanceUID, tsuid);

                    IOException ioexception = null;
                    final HttpURLConnection c = (HttpURLConnection)importURL.openConnection();
                    try {
                        c.setRequestMethod("POST");
                        c.setDoInput(true);
                        c.setDoOutput(true);
                        final DicomOutputStream dos = new DicomOutputStream(c.getOutputStream());
                        // TODO: bandwidth limitation?
                        try {
                            dos.writeFileMetaInformation(fmi);
                            dos.writeDataset(o, tsuid);
                        } catch (IOException e) {
                            throw ioexception = e;
                        } finally {
                            try {
                                dos.close();
                            } catch (IOException e) {
                                throw ioexception = null == ioexception ? e : ioexception;
                            }
                        }
                        if (c.getResponseCode() >= 400) {
                            System.out.println(String.format("Request FAILED for %s series %d instance %d: %s",
                                    studyInstanceUID, series, instance, c.getResponseMessage()));
                            final BufferedReader reader = new BufferedReader(new InputStreamReader(c.getErrorStream()));
                            try {
                                String line;
                                while (null != (line = reader.readLine())) {
                                    System.out.println(line);
                                }
                                result.setSuccessful(false);
                                return result;
                            } catch (IOException e) {
                                throw ioexception = e;
                            } finally {
                                try {
                                    reader.close();
                                } catch (IOException e) {
                                    throw ioexception = null == ioexception ? e : ioexception;
                                }
                            }
                        } else {
                            uris.addAll(readLines(c.getInputStream()));
                        }
                    } finally {
                        c.disconnect();
                    }
                }
            }
            if (getDoesCommit()) {
                for (final String uri : uris) {
                    final StringBuilder sb = new StringBuilder(getBaseURL());
                    if ('/' != uri.charAt(0)) {
                        sb.append("/");
                    }
                    sb.append(uri);
                    sb.append("?action=commit");
                    final URL commitURL = new URL(sb.toString());
                    IOException ioexception = null;
                    final HttpURLConnection c = (HttpURLConnection)commitURL.openConnection();
                    try {
                        c.setRequestMethod("POST");
                        if (c.getResponseCode() >= 400) {
                            System.out.println("commit request for " + uri + " FAILED: " + c.getResponseMessage());
                            final BufferedReader reader = new BufferedReader(new InputStreamReader(c.getErrorStream()));
                            try {
                                String line;
                                while (null != (line = reader.readLine())) {
                                    System.out.println(line);
                                }
                            } catch (IOException e) {
                                throw ioexception = e;
                            } finally {
                                try {
                                    reader.close();
                                } catch (IOException e) {
                                    throw ioexception = null == ioexception ? e : ioexception;
                                }
                            }
                            result.setSuccessful(false);
                            return result;
                        }
                    } finally {
                        c.disconnect();
                    }
                }
            }
            System.out.println("response URIs " + uris);
            result.setResponseData(Joiner.on(",").join(uris), Charset.defaultCharset().name());
            result.setResponseCodeOK();
            result.setSuccessful(true);

            long size = 0;
            int totalCount = 0;
            for (int seriesCount : counts) {
                size += seriesCount * getTemplateObjectSize();
                totalCount += seriesCount;
            }
            result.setBytes((int)size);
            result.setSampleCount(totalCount);
        } catch (Throwable t) {
            System.err.println("failed to send DICOM file");
            t.printStackTrace(System.err);
            logger.info("unable to send DICOM file", t);
            result.setSuccessful(false);
        } finally {
            result.sampleEnd();
        }
        return result;
    }
}
