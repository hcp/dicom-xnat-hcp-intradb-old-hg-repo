/**
 * Copyright (c) 2014 Washington University School of Medicine
 */
package org.nrg.dcm;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.SiemensShadowHeader.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

/**
 * Indexes to access fields defined in Siemens Phoenix Protocol header
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class SiemensPhoenixProtocolAttributeIndex implements DicomAttributeIndex {
    private Logger logger = LoggerFactory.getLogger(SiemensPhoenixProtocolAttributeIndex.class);
    private final String colName, attrName;
    private final int[] tagPath;

    protected SiemensPhoenixProtocolAttributeIndex(final String colName, final String attrName, int...tagPath) {
        this.colName = colName;
        this.attrName = attrName;
        this.tagPath = tagPath;
    }
    
    protected SiemensPhoenixProtocolAttributeIndex(final String colName, final String attrName) {
        this(colName, attrName, SiemensPrivateAttributes.TAG_SIEMENS_IMAGE_SERIES_SHADOW_HEADER);
    }

    public String getAttributeName(final DicomObject _) {
        return attrName;
    }

    public String getColumnName() {
        return colName;
    }

    public DicomElement getElement(final DicomObject o) {
        return o.get(tagPath);
    }

    public Integer[] getPath(final DicomObject _) { 
        final Integer[] tp = new Integer[tagPath.length];
        for (int i = 0; i < tagPath.length; i++) {
            tp[i] = tagPath[i];
        }
        return tp;
    }

    public String getString(final DicomObject o, final String defaultv) {
        try {
            final String v = getString(o);
            return null == v ? defaultv : v;
        } catch (ConversionFailureException e) {
            logger.info("shadow header element extraction failed", e);
            return null;
        } catch (Throwable t) {
            final String message = String.format("unable to parse phoenix protocol attribute %s/%s in %s",
                    DicomAttributes.showTagPath(tagPath), attrName, o.getString(Tag.SOPInstanceUID));
            logger.error(message, t);
            return null;
        }
    }

    public String[] getStrings(DicomObject o) {
        try {
            final String v = getString(o);
            return null == v ? null : v.split("\\");
        } catch (ConversionFailureException e) {
            logger.info("shadow header element extraction failed", e);
            return null;
        }
    }

    public String getString(DicomObject o) throws ConversionFailureException {
        final List<Field> field = SiemensShadowHeader.getFields(o, getElement(o),
                Collections.singleton(Predicates.equalTo("MrPhoenixProtocol")));
        if (null == field || field.isEmpty()) {
            return null;
        }

        final String mrPhoenix = field.get(0).getValue();
        final int ascconv0 = mrPhoenix.indexOf("### ASCCONV BEGIN");
        if (ascconv0 < 0) {
            logger.warn("no ASCCONV found in Siemens MR Phoenix");
            return null;
        }
        final int props0 = mrPhoenix.indexOf("\n", ascconv0) + 2;
        if (props0 < 2) {
            logger.warn("no CR after start of Siemens MR Phoenix ASCCONV");
            return null;
        }
        final int ascconv1 = mrPhoenix.indexOf("### ASCCONV END", props0);
        if (ascconv1 < 0) {
            logger.warn("no end to Siemens MR Phoenix ASCCONV");
            return null;
        }
        final String proptext = mrPhoenix.substring(props0, ascconv1)
                .replaceAll("\\n", "\n").replaceAll("\\t", "\t");
        final Properties props = new Properties();
        try {
            props.load(new StringReader(proptext));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return getString(props);
    }

    protected abstract String getString(Map<?,?> fields);
    
    /**
     * Phoenix Protocol attribute that has a single, fixed name.
     * @author Kevin A. Archie <karchie@wustl.edu>
     *
     */
    public static class Named extends SiemensPhoenixProtocolAttributeIndex {
        private final String name;
        
        public Named(final String colName, final String attrName, final String fieldName) {
            super(colName, attrName);
            this.name = fieldName;
        }
        
        protected final String getString(final Map<?,?> fields) {
            final Object v = fields.get(name);
            return null == v ? null : v.toString();
        }
    }
    
    /**
     * Phoenix Protocol attribute that could appear with various (enumerated) names.
     * Field names are listed in decreasing precedence order.
     * @author Kevin A. Archie <karchie@wustl.edu>
     *
     */
    public static class Aliased extends SiemensPhoenixProtocolAttributeIndex {
        private final ImmutableList<String> aliases;
        
        public Aliased(final String colName, final String attrName,
                final String alias1, final String alias2, String...aliases) {
            super(colName, attrName);
            final ImmutableList.Builder<String> builder = ImmutableList.builder();
            builder.add(alias1);
            builder.add(alias2);
            builder.addAll(Arrays.asList(aliases));
            this.aliases = builder.build();
        }
        
        protected final String getString(final Map<?,?> fields) {
            for (final String name : aliases) {
                final Object val = fields.get(name);
                if (null != val) {
                    return val.toString().trim();
                }
            }
            return null;
        }
    }
    
    /**
     * Special case of AlternativeNamed; we use this one a lot. The fields are named
     * sWiPMemBlock[i] on VBxx (Trio) and sWipMemBlock[i] on VDxx (Skyra, Prisma).
     * @author Kevin A. Archie <karchie@wustl.edu>
     *
     */
    public static final class SWipMemBlock extends Aliased {
        public SWipMemBlock(final String colName, final String fieldName, final int index) {
            super(colName, fieldName,
                    String.format("sWipMemBlock.alFree[%d]", index),
                    String.format("sWiPMemBlock.alFree[%d]", index));

        }

        public SWipMemBlock(final int index) {
            this(String.format("Siemens_sWipMemBlock_alFree_%d", index),
                    String.format("Siemens sWipMemBlock.alFree[%d]", index),
                    index);
        }
    }
    
    /**
     * Special case of AlternativeNamed; we use this one a lot. The fields are named
     * sWiPMemBlock[i] on VBxx (Trio) and sWipMemBlock[i] on VDxx (Skyra, Prisma).
     * @author Kevin A. Archie <karchie@wustl.edu>
     *
     */
    public static final class SWipMemBlockAd extends Aliased {
        public SWipMemBlockAd(final String colName, final String fieldName, final int index) {
            super(colName, fieldName,
                    String.format("sWipMemBlock.adFree[%d]", index),
                    String.format("sWiPMemBlock.adFree[%d]", index));

        }

        public SWipMemBlockAd(final int index) {
            this(String.format("Siemens_sWipMemBlock_adFree_%d", index),
                    String.format("Siemens sWipMemBlock.adFree[%d]", index),
                    index);
        }
    }

    
    /**
     * Composite attribute combines all the named field values, separated by \ character.
     * All named fields get an entry in the value, even if not defined in the header: for
     * example, if 3 field names are provided, the value will always contain two \ separators,
     * possibly delimiting empty fields.
     * @author Kevin A. Archie <karchie@wustl.edu>
     *
     */
    public static final class JoinedList extends SiemensPhoenixProtocolAttributeIndex {
        private final ImmutableMap<Object,Integer> fieldNames;

        public JoinedList(final String colName, final String attrName, final String...fieldNames) {
            super(colName, attrName);
            final ImmutableMap.Builder<Object,Integer> fnbuilder = ImmutableMap.builder();
                   
            int i = 0;
            for (final String fieldName : fieldNames) {
                fnbuilder.put(fieldName, i++);
            }
            this.fieldNames = fnbuilder.build();
        }

        protected String getString(final Map<?,?> fields) {
            final String[] offsets = new String[fieldNames.size()];
            boolean hasNonEmptyOffset = false;
            for (int i = 0; i < fieldNames.size(); i++) {
                offsets[i] = "";
            }
            for (final Map.Entry<?,?> me : fields.entrySet()) {
                final Integer i = fieldNames.get(me.getKey());
                if (null != i) {
                    offsets[i] = me.getValue().toString().trim();
                    if (!hasNonEmptyOffset && offsets[i].length()>0) {
                    	hasNonEmptyOffset = true;
                    }
                }
            }
            // We'll return an empty value if all offsets are missing so missing data here is treated
            // like other missing fields.
            return (hasNonEmptyOffset) ? Joiner.on('\\').join(offsets) : "";
        }
    }
    
    private static final ImmutableList<DicomAttributeIndex> buildPositioningAttributes(final String prefix) {
        final ImmutableList.Builder<DicomAttributeIndex> builder = ImmutableList.builder();
        final List<String> vars = Lists.newArrayList("dThickness", "dPhaseFOV", "dReadoutFOV", "dInPlaneRot");
        for (final String group : ImmutableList.of("sPosition", "sNormal")) {
            for (final String orient : ImmutableList.of("dSag", "dCor", "dTra")) {
                vars.add(group+"."+orient);
            }
        }
        for (final String var : vars) {
            final String colName = ("Siemens_"+prefix+"_"+var).replaceAll("[^\\w_]", "_");
            final String attrName = "Siemens "+prefix+"."+var;
            builder.add(new Named(colName, attrName, prefix + "." + var));
        }
        return builder.build();
    }
    
    public static final ImmutableList<DicomAttributeIndex>
    FOV_POSITION = buildPositioningAttributes("sSliceArray.asSlice[0]"),
    ADJUST_VOLUME_POSITION = buildPositioningAttributes("sAdjData.sAdjVolume"), 
    NAVIGATOR_POSITION = buildPositioningAttributes("sNavigatorArray.asElm[0].sCuboid");
}

