/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.nrg.dcm.SiemensPrivateAttributes.*;

/**
 * Echo spacing for Siemens scanners.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MREchoSpacingAttribute extends Abstract<String> implements XnatAttrDef {
    private final static Pattern acqMatrixPattern = Pattern.compile("\\A(\\d|[1-9]\\d+)p?\\*(\\d|[1-9]\\d+)s?\\z");
    private final Logger logger = LoggerFactory.getLogger(MREchoSpacingAttribute.class);

    public MREchoSpacingAttribute() {
        super("parameters/echoSpacing", SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE, SIEMENS_ACQUISITION_MATRIX_TEXT);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.Foldable#start()
     */
    public String start() { return null; }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#foldl(java.lang.Object, java.util.Map)
     */
    public String foldl(final String a, final Map<? extends DicomAttributeIndex,? extends String> m)
            throws ExtAttrException {
        final String v = decode(m);
        if (null == v) {
            return a;
        } else if (null == a || a.equals(v)) {
            return v;
        } else {
            throw new NoUniqueValueException(getName(), Arrays.asList(a, v));
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#apply(java.lang.Object)
     */
    public Iterable<ExtAttrValue> apply(final String a) throws ExtAttrException {
        return applyString(a);
    }

    private String decode(final Map<? extends DicomAttributeIndex,? extends String> m)
            throws ConversionFailureException {
        logger.trace("decoding {}", m);
        final double bpppe;
        final int nsamples;
        try {
            bpppe = Double.valueOf(m.get(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE));
        } catch (Throwable t) {
            logger.debug("conversion failed on BandwidthPerPixelPhaseEncode", t);
            throw new ConversionFailureException("BandwidthPerPixelPhaseEncode",
                    m.get(SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE),
                    "Bandwidth per Pixel Phase Encode (0019,1028) needed for parameters/echoSpacing", t);
        }
        final Matcher matcher;
        try {
            matcher = acqMatrixPattern.matcher(m.get(SIEMENS_ACQUISITION_MATRIX_TEXT));
        } catch (Throwable t) {
            logger.debug("unable to parse Siemens Acquisition Matrix (0051,100b): " + 
                    m.get(SIEMENS_ACQUISITION_MATRIX_TEXT), t);
            throw new ConversionFailureException("AcquisitionMatrixText",
                    m.get(SIEMENS_ACQUISITION_MATRIX_TEXT),
                    "Acquisition Matrix (0051,100b) needed for parameters/echoSpacing", t);
        }
        if (matcher.matches()) {
            try {
                nsamples = Integer.valueOf(matcher.group(1));
            } catch (NumberFormatException e) {
                throw new RuntimeException("unable to convert [1-9]*\\d to integer?", e);
            } catch (Throwable t) {
                logger.debug("conversion failed on AcquisitionMatrixText", t);
                throw new ConversionFailureException("AcquisitionMatrixText",
                        m.get(SIEMENS_ACQUISITION_MATRIX_TEXT),
                        "Acquisition Matrix (0051,100b) needed for parameters/echoSpacing", t);
            }
        } else {
            throw new ConversionFailureException("AcquisitionMatrixText",
                    m.get(SIEMENS_ACQUISITION_MATRIX_TEXT),
                    "Acquisition Matrix (0051,100b) needed for parameters/echoSpacing");
        }
        try {
            return Double.toString(1/(bpppe * nsamples));
        } catch (Throwable t) {
            throw new ConversionFailureException("parameters/echoSpacing",
                    String.format("1/(%g * %d)", bpppe, nsamples),
                    "error computing value", t);
        }
    }
}
