/**
 * Copyright 2012 Washington University
 */
package org.nrg.dcm.xnat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class AbstractDateTimeAttribute extends Abstract<java.util.Date> implements XnatAttrDef {
    private static final Pattern DICOM_DATETIME = Pattern.compile("(\\d{14})(?:(?:\\.)(\\d{1,6}))?");

    private static final String DICOM_DT_FORMAT = "yyyyMMddHHmmss'.'SSS";

    /**
     * Normalizes the input, which may contain 0-6 decimal places of fractional seconds,
     * so that it always indicates 3 places (ms precision), and converts this normalized
     * form to a java.util.Date. Ignores TZ suffix.
     * @param dt DICOM DT string (possibly constructed from distinct DA and TM)
     * @return
     */
    protected final static java.util.Date parseDateTime(final String dt) {
        if (Strings.isNullOrEmpty(dt)) {
            return null;
        }
        final Matcher match = DICOM_DATETIME.matcher(dt);
        if (!match.matches()) {
            return null;
        }
        final StringBuilder canonical = new StringBuilder(match.group(1));
        final String msraw = match.group(2);
        final String ms = String.format("%3s", null == msraw ? "" : msraw).substring(0,3).replace(' ', '0');
        canonical.append('.').append(ms);

        try {
            final DateFormat dtFormat = new SimpleDateFormat(DICOM_DT_FORMAT);
            return dtFormat.parse(canonical.toString());
        } catch (ParseException e) {
            LoggerFactory.getLogger(ScanStartTimeAttrDef.class).error("unable to parse scan time", e);
            return null;
        } 
    }

    private final SimpleDateFormat XSD_TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

    public AbstractDateTimeAttribute(final String name, final DicomAttributeIndex...attrs) {
        super(name, attrs);
    }

    /**
     * apply method for xs:time output
     * @param a final fold accumulator
     * @return xs:time representation of accumulator value
     * @throws ExtAttrException
     */
    public Iterable<ExtAttrValue> applyTime(final java.util.Date a) throws ExtAttrException {
        if (null == a) {
            return null;
        } else {
            return applyString(XSD_TIME_FORMAT.format(a));
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.Foldable#start()
     */
    public java.util.Date start() { return null; }
}
