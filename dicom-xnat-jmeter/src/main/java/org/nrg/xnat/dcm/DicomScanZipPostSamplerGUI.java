/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jmeter.testelement.TestElement;

/**
 * @author Kevin A. Archie (karchie@wustl.edu)
 *
 */
public final class DicomScanZipPostSamplerGUI extends AbstractSamplerGui {
    private static final long serialVersionUID = 1L;

    private final JLabel baseURLLabel = new JLabel("Base URL of test XNAT:"),
    importURILabel = new JLabel("Relative URI of DICOM import service"),
    userLabel = new JLabel("Test service user:"),
    passLabel = new JLabel("Test service password:"),
    templateFileLabel = new JLabel("path to sample DICOM file:"),
    fileCountLabel = new JLabel("series counts (comma-separated):"),
    doesCommitLabel = new JLabel("commit session after upload?"),
    useFixedSizeLabel = new JLabel("use fixed size streaming?"),
    numThreadsLabel = new JLabel("number of sending threads");
    
    private final JCheckBox doesCommitCheckbox = new JCheckBox(),
    useFixedSizeCheckBox = new JCheckBox();

    private final JTextField baseURLField = new JTextField(),
    importURIField = new JTextField(),
    userField = new JTextField(),
    passField = new JTextField(),
    templateFileField = new JTextField(),
    fileCountField = new JTextField(),
    numThreadsField = new JTextField();

    /**
     * 
     */
    public DicomScanZipPostSamplerGUI() {
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());
        add(makeTitlePanel(), BorderLayout.NORTH);
        add(makeParameterPanel(), BorderLayout.CENTER);
    }

    private Component makeParameterPanel() {
        final JPanel panel = new JPanel(new GridLayout(9, 2));
        panel.add(baseURLLabel);
        panel.add(baseURLField);
        baseURLLabel.setLabelFor(baseURLField);
        panel.add(importURILabel);
        panel.add(importURIField);
        importURILabel.setLabelFor(importURIField);
        panel.add(userLabel);
        panel.add(userField);
        userLabel.setLabelFor(userField);
        panel.add(passLabel);
        panel.add(passField);
        passLabel.setLabelFor(passField);
        panel.add(templateFileLabel);
        panel.add(templateFileField);
        templateFileLabel.setLabelFor(templateFileField);
        panel.add(fileCountLabel);
        panel.add(fileCountField);
        fileCountLabel.setLabelFor(fileCountField);
        panel.add(doesCommitLabel);
        panel.add(doesCommitCheckbox);
        doesCommitLabel.setLabelFor(doesCommitCheckbox);
        panel.add(useFixedSizeLabel);
        panel.add(useFixedSizeCheckBox);
        useFixedSizeLabel.setLabelFor(useFixedSizeCheckBox);
        panel.add(numThreadsLabel);
        panel.add(numThreadsField);
        numThreadsLabel.setLabelFor(numThreadsField);
        
        return panel;
    }

    public void configure(final TestElement te) {
        super.configure(te);
        final DicomScanZipPostSampler sampler = (DicomScanZipPostSampler)te;
        baseURLField.setText(sampler.getBaseURL());
        importURIField.setText(sampler.getRequestURI());
        userField.setText(sampler.getUser());
        passField.setText(sampler.getPassword());
        templateFileField.setText(sampler.getTemplate());
        fileCountField.setText(sampler.getCounts());
        doesCommitCheckbox.setSelected(sampler.getDoesCommit());
        useFixedSizeCheckBox.setSelected(sampler.getUseFixedSize());
        numThreadsField.setText(Integer.toString(sampler.getNumThreads()));
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.gui.JMeterGUIComponent#createTestElement()
     */
    public TestElement createTestElement() {
        final DicomScanZipPostSampler sampler = new DicomScanZipPostSampler();
        modifyTestElement(sampler);
        return sampler;
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.gui.JMeterGUIComponent#getLabelResource()
     */
    public String getLabelResource() {
        return "dicom_scan_zip_post_sampler";
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.gui.JMeterGUIComponent#modifyTestElement(org.apache.jmeter.testelement.TestElement)
     */
    public void modifyTestElement(final TestElement te) {
        super.configureTestElement(te);
        final DicomScanZipPostSampler sampler = (DicomScanZipPostSampler)te;
        sampler.setBaseURL(baseURLField.getText());
        sampler.setImportURI(importURIField.getText());
        sampler.setUser(userField.getText());
        sampler.setPassword(passField.getText());
        sampler.setTemplate(templateFileField.getText());
        sampler.setCounts(fileCountField.getText()); 
        sampler.setDoesCommit(doesCommitCheckbox.isSelected());
        sampler.setUseFixedSize(useFixedSizeCheckBox.isSelected());
        int nThreads;
        try {
        nThreads = Integer.parseInt(numThreadsField.getText());
        } catch (NumberFormatException e) {
            nThreads = 1;
        }
        sampler.setNumThreads(nThreads);
    }
}
