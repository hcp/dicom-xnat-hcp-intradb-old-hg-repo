/**
 * Copyright 2012 Washington University
 */
package org.hcp.dcm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.nrg.dcm.Attributes.EchoNumbers;
import static org.nrg.dcm.Attributes.SeriesDescription;

import org.junit.Test;
import org.nrg.attr.ExtAttrException;
import org.nrg.dcm.DicomAttributeIndex;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.SetMultimap;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class HCPSeriesDescriptionTest {
    @Test
    public void testGetAttrName() {
        assertEquals("series_description", new HCPSeriesDescription().getName());
    }

    /**
     * Test method for {@link org.hcp.dcm.HCPSeriesDescription#apply(com.google.common.collect.SetMultimap)}.
     */
    @Test
    public void testApply() throws ExtAttrException {
        final HCPSeriesDescription sd = new HCPSeriesDescription();
        assertEquals("foo", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "foo")
                .put(EchoNumbers, "1")
                .build()).iterator().next().getText());

        assertEquals("fieldmap", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "fieldmap")
                .build()).iterator().next().getText());

        assertEquals("fieldmap_Magnitude", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "fieldmap")
                .put(EchoNumbers, "1")
                .put(EchoNumbers, "2")
                .build()).iterator().next().getText());

        assertEquals("fieldmap025_Phase", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "fieldmap025")
                .put(EchoNumbers, "2")
                .build()).iterator().next().getText());
        
        assertEquals("fieldmap_foo", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "fieldmap_foo")
                .put(EchoNumbers, "2")
                .build()).iterator().next().getText());
    }

    @Test
    public void testApplyMultipleDescs() throws ExtAttrException {
        final HCPSeriesDescription sd = new HCPSeriesDescription();
        assertFalse("foo", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "foo")
                .put(SeriesDescription, "bar")
                .put(EchoNumbers,"1")
                .build()).iterator().hasNext());       
    }

    @Test
    public void testApplyNoDesc() throws ExtAttrException {
        final HCPSeriesDescription sd = new HCPSeriesDescription();
        assertFalse("foo", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(EchoNumbers, "1")
                .build()).iterator().hasNext());       
    }

    @Test
    public void testApplyEmptyDesc() throws ExtAttrException {
        final HCPSeriesDescription sd = new HCPSeriesDescription();
        assertFalse("foo", sd.apply(ImmutableSetMultimap.<DicomAttributeIndex,String>builder()
                .put(SeriesDescription, "")
                .put(EchoNumbers,"1")
                .build()).iterator().hasNext());       
    }


    /**
     * Test method for {@link org.hcp.dcm.HCPSeriesDescription#foldl(com.google.common.collect.SetMultimap, java.util.Map)}.
     */
    @Test
    public void testFoldlSetMultimapOfDicomAttributeIndexStringMapOfQextendsDicomAttributeIndexQextendsString() {
        final HCPSeriesDescription sd = new HCPSeriesDescription();
        final SetMultimap<DicomAttributeIndex,String> m0 = sd.start();
        assertTrue(m0.isEmpty());
        final SetMultimap<DicomAttributeIndex,String> m1 = sd.foldl(m0,
                ImmutableMap.of(SeriesDescription, "foo", EchoNumbers, "1"));
        assertFalse(m1.isEmpty());
    }

}
