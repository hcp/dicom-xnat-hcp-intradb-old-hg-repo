/**
 * Copyright (c) 2011,2013 Washington University
 */
package org.nrg.dcm;

import org.dcm4che2.data.DicomObject;
import org.nrg.dcm.SiemensShadowHeaderAttributeIndex.Type;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SiemensPrivateAttributes {
    private SiemensPrivateAttributes() {}

    public static final int
    TAG_SIEMENS_IMAGE_INSTANCE_SHADOW_HEADER = 0x00291010,
    TAG_SIEMENS_IMAGE_SERIES_SHADOW_HEADER = 0x00291020;

    public static final DicomAttributeIndex
    SIEMENS_BANDWIDTH_PER_PIXEL_PHASE_ENCODE = new FixedDicomAttributeIndex(0x00191028) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Bandwidth per pixel phase encode";
        }
    },
    SIEMENS_ACQUISITION_MATRIX_TEXT = new FixedDicomAttributeIndex(0x0051100b) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Acquisition Matrix Text";
        }
    },
    SIEMENS_TABLE_POSITION = new FixedDicomAttributeIndex(0x00191014) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Table Position";
        }
    },
    SIEMENS_DIFF_B_VALUE = new FixedDicomAttributeIndex(0x0019100c) {
        public String getAttributeName(DicomObject o) {
            return "Siemens b Value";
        }
    },
    SIEMENS_ORIENTATION_TEXT = new FixedDicomAttributeIndex(0x0051100e) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Orientation Text";
        }
    },
    SIEMENS_READOUT_SAMPLE_SPACING = new FixedDicomAttributeIndex(0x00191018) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Readout Sample Spacing";
        }
    },
    SIEMENS_MOSAIC_SLICE_COUNT = new FixedDicomAttributeIndex(0x0019100a) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Mosaic Slice Count";
        }
    },
    SIEMENS_COIL_STRING = new FixedDicomAttributeIndex(0x0051100f) {
        public String getAttributeName(DicomObject o) {
            return "Siemens Coil String";
        }
    };

    public static final DicomAttributeIndex SIEMENS_GRADSPEC_LOFFSET =
            new SiemensPhoenixProtocolAttributeIndex.JoinedList("Siemens_GRADSPEC_lOffset",
                    "Siemens GRADSPEC lOffset",
                    "sGRADSPEC.lOffsetX", "sGRADSPEC.lOffsetY", "sGRADSPEC.lOffsetZ");

    // NOTE:  The fields for this attribute were changed for the Prisma.  He're we'll assign this (VE11) version.
    public static final DicomAttributeIndex SIEMENS_GRADSPEC_LOFFSET_VE11 =
            new SiemensPhoenixProtocolAttributeIndex.JoinedList("Siemens_GRADSPEC_lOffset_ve11",
                    "Siemens GRADSPEC lOffset",
                    "sGRADSPEC.asGPAData[0].lOffsetX", "sGRADSPEC.asGPAData[0].lOffsetY", "sGRADSPEC.asGPAData[0].lOffsetZ");

    public static final DicomAttributeIndex SIEMENS_GRADSPEC_ALSHIMCURRENT =
            new SiemensPhoenixProtocolAttributeIndex.JoinedList("Siemens_GRADSPEC_alShimCurrent",
                    "Siemens GRADSPEC alShimCurrent",
                    "sGRADSPEC.alShimCurrent[0]", "sGRADSPEC.alShimCurrent[1]", "sGRADSPEC.alShimCurrent[2]",
                    "sGRADSPEC.alShimCurrent[3]", "sGRADSPEC.alShimCurrent[4]");

    public static final DicomAttributeIndex SIEMENS_IN_PLANE_ROT =
            new SiemensPhoenixProtocolAttributeIndex.JoinedList("Siemens_sSliceArray_0_dInPlaneRot",
                    "Siemens Slice Array 0 dInPlaneRot", "sSliceArray.asSlice[0].dInPlaneRot");
    
    public static final DicomAttributeIndex SIEMENS_DIFF_REFOCUS_FLIP_ANGLE =
            new SiemensPhoenixProtocolAttributeIndex.JoinedList("Siemens_dMRI_Refocus_Flip_Angle",
                    "Siemens dMRI Refocusing Pulse Flip Angle", "adFlipAngleDegree[1]");
    
    public static final DicomAttributeIndex SIEMENS_SWIPMEMBLOCK_ALFREE_0 = new SiemensPhoenixProtocolAttributeIndex.SWipMemBlock(0);
    public static final DicomAttributeIndex SIEMENS_SWIPMEMBLOCK_ALFREE_16 = new SiemensPhoenixProtocolAttributeIndex.SWipMemBlock(16);
    
    public static final DicomAttributeIndex SIEMENS_PHASE_ENCODING_DIRECTION_POSITIVE =
            new SiemensShadowHeaderAttributeIndex("PhaseEncodingDirectionPositive", Type.INSTANCE);
    
    public static final DicomAttributeIndex SIEMENS_iPAT_FACTOR =
            new SiemensPhoenixProtocolAttributeIndex.Named("Siemens_iPAT_factor", "Siemens iPAT factor", "sPat.lAccelFactPE");
    
    public static final DicomAttributeIndex SIEMENS_iPAT_N_REF_LINES =
            new SiemensPhoenixProtocolAttributeIndex.Named("Siemens_iPAT_N_REF_LINES", "Siemens iPAT number of reference lines", "sPat.lRefLinesPE");
    
    public static final DicomAttributeIndex SIEMENS_TX_REF_AMP =
            new SiemensPhoenixProtocolAttributeIndex.Named("Siemens_txRefAmp", "Siemens txRefAmp", "sTXSPEC.asNucleusInfo[0].flReferenceAmplitude");
    
    public static final DicomAttributeIndex SIEMENS_T_MEASURED_BASELINE_STRING =
            new SiemensPhoenixProtocolAttributeIndex.Named("Siemens_tMeasuredBaselineString", "Siemens sProtConsistencyInfo.tMeasuredBaselineString", "sProtConsistencyInfo.tMeasuredBaselineString");
}
