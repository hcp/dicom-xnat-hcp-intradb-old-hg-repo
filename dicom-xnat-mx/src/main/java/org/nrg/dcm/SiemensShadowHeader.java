/**
 * Copyright (c) 2012,2013 Washington University School of Medicine
 */
package org.nrg.dcm;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.nrg.attr.ConversionFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;

/**
 * Parses component fields out of a Siemens shadow header (vendor private tag)
 *
 */
public class SiemensShadowHeader {    
    private static final String IMAGE_NUM_4 = "IMAGE NUM 4",
    SIEMENS_CSA_HEADER = "SIEMENS CSA HEADER",
    SPEC_NUM_4 = "SPEC NUM 4",
    SIEMENS_CSA_NON_IMAGE = "SIEMENS CSA NON-IMAGE",
    SV10_MAGIC = "SV10";

    private static final int INT32_SIZE = 4;

    private static String getStringZ(final byte[] bytes, final int offset, final int length) {
        for (int i = 0; i < length; i++) {
            if (0 == bytes[offset+i]) {
                return new String(bytes, offset, i);
            }
        }
        return new String(bytes, offset, length);
    }
    
    private static int getLEint(final ByteBuffer bb, final int offset) {
        try {
            return Integer.reverseBytes(bb.getInt(offset));
        } catch (RuntimeException e) {
            LoggerFactory.getLogger(SiemensShadowHeader.class).error("failed to read LE integer", e);
            throw e;
        } catch (Error e) {
            LoggerFactory.getLogger(SiemensShadowHeader.class).error("failed to read LE integer", e);
            throw e;
        }
    }

    public static boolean isShadowHeader(final DicomObject o, final DicomElement e) {
        final int tag = e.tag();
        if (0x00290000 != (tag & 0xffff0000)) {
            return false;
        }
        final String version = o.getString(0x00291008);
        final String csa = o.getString(0x00290010);

        if (IMAGE_NUM_4.equals(version) && SIEMENS_CSA_HEADER.equals(csa)
                && (tag == 0x00291010 || tag == 0x00291020)) {
            return true;
        } else if (SPEC_NUM_4.equals(version) && SIEMENS_CSA_NON_IMAGE.equals(csa)
                && (tag == 0x00291210 || tag == 0x00291220
                        || tag == 0x00291110 || tag == 0x00291120)) {
            return true;
        } else {
            return false;
        }
    }

    public static class Field {
        private final String field, description;
        private final String vr;
        private final String value;
        
        private Field(final String field, final String vr, final String description,
                final String value) {
            this.field = field;
            this.vr = vr;
            this.description = description;
            this.value = value;
        }
        
        public String getField() { return field; }
        public String getVR() { return vr; }
        public String getDescription() { return description; }
        public String getValue() { return value; }
        
        @Override
        public boolean equals(final Object o) {
            if (null != o && o instanceof Field) {
                final Field other = (Field)o;
                return Objects.equal(field, other.field) &&
                Objects.equal(description, other.description) &&
                Objects.equal(vr, other.vr) &&
                Objects.equal(value, other.value);
            } else {
                return false;
            }
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(field, description, vr, value);
        }
    }
    
    public static List<Field>
    getFields(final DicomObject o, final DicomElement elem, final Collection<Predicate<String>> only)
    throws ConversionFailureException {
        if (null == elem) {
            throw new ConversionFailureException(o, null, "Siemens shadow header element not found");
        }
        final byte[] bytes = elem.getBytes();
        final ByteBuffer bb = ByteBuffer.wrap(bytes);
        if (!SV10_MAGIC.equals(new String(bytes, 0, 4))) {
            throw new ConversionFailureException(elem, new String(bytes),
                    "not a Siemens shadow header: wrong magic");
        }
        final int tag = elem.tag();
        final String version = o.getString(0x00291008);
        final String csa = o.getString(0x00290010);
        final String desc;
        if (IMAGE_NUM_4.equals(version)) {
            if (SIEMENS_CSA_HEADER.equals(csa)) {
                if (tag == 0x00291010) {
                    desc = "Siemens CSA Image Instance Shadow Header";
                } else if (tag == 0x00291020) {
                    desc = "Siemens CSA Image Series Shadow Header";
                } else {
                    desc = "Siemens CSA Unknown Image Shadow Header";
                }
            } else {
                throw new ConversionFailureException(elem, csa, "invalid Siemens CSA HEADER identifier");
            }
        } else if (SPEC_NUM_4.equals(version)) {
            if (SIEMENS_CSA_NON_IMAGE.equals(csa)) {
                if (tag == 0x00291210) {
                    desc = "Siemens CSA Non-Image Instance Shadow Header";
                } else if (tag == 0x00291220) {
                    desc = "Siemens CSA Non-Image Series Shadow Header";
                } else if (tag == 0x00291110) {
                    desc = "Siemens CSA VB13 Instance Shadow Header";
                } else if (tag == 0x00291120) {
                    desc = "Siemens CSA VB13 Series Shadow Header";
                } else {
                    desc = "Siemens CSA Unknown Non-Image Shadow Header";
                }
            } else {
                throw new ConversionFailureException(elem, csa, "invalid Siemens CSA HEADER identifier");
            }
        } else {
            throw new ConversionFailureException(elem, version, "invalid NUMARIS version");
        }

        final Logger logger = LoggerFactory.getLogger(SiemensShadowHeader.class);
        
        final List<Field> fields = Lists.newArrayList();
        final int n = getLEint(bb, 8);
        logger.trace("shadow header has {} elements", n);
        int offset = 16;    // skip unknown int32 (= 77)
        for (int i = 0; i < n; i++) {
            final String fieldName = getStringZ(bytes, offset, 64); offset += 64;
            int vm = getLEint(bb, offset); offset += INT32_SIZE;
            final String vr = getStringZ(bytes, offset, 4); offset += 4;
            offset += INT32_SIZE;    // skip SyngoDT (int32)
            final int numItems = getLEint(bb, offset);  offset += INT32_SIZE;
            if (0 == vm) {
                vm = numItems; // can happen in spectroscopy or VB13 images
            }
            logger.trace("parsing shadow element {} {}x{} ({}) @ {}", new Object[]{fieldName, vm, vr, numItems, Integer.toHexString(offset)});

            if (vm > numItems && numItems > 0) {
                logger.warn("VM ({}) > numItems ({})", vm, numItems);
            }
            
            int succZeroWidth = 0;
            final String value;
            if (numItems > 1) {
                final Collection<String> values = Lists.newArrayListWithExpectedSize(numItems);
                offset += INT32_SIZE;   // skip unknown int32 (= 77)
                for (int j = 0; j < vm && j < numItems; j++) {
                    offset += 3 * INT32_SIZE;   // skip three int32s
                    final int fieldWidth = 4*(int)Math.ceil(getLEint(bb, offset)/4.0); offset += INT32_SIZE;
                    if (0 == fieldWidth) {
                        succZeroWidth++;
                        logger.debug("{}th zero width field", succZeroWidth);
                    } else {
                        succZeroWidth = 0;
                    }
                    final String v = getStringZ(bytes, offset, fieldWidth).trim();
                    logger.trace("  offset {} field {} width {} value {}", new Object[]{Integer.toHexString(offset), j, fieldWidth, v});
                    values.add(v);
                    offset += fieldWidth;
                }
                value = Joiner.on("\\").join(values);
            } else {
                value = "";
            }

            // skip the junk bytes at the end of the item
            if (succZeroWidth > 3) {
                // there's this one file from Mt. Sinai. Not sure exactly what
                // the conditions are, but this fixes that one file.
                offset += 0;
            } else if (numItems < 1) {
                offset += 4;
            } else if (numItems < vm) {
                offset += 16;
            } else {
                offset += 16 * (numItems - vm);
            }

            if (null == only || only.isEmpty() || Predicates.or(only).apply(fieldName)) {
                fields.add(new Field(fieldName, vr, desc, value));
            }
        }
        return fields;
    }
}
