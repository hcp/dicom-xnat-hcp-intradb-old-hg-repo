/**
 * Copyright 2012 Washington University
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.AcquisitionDate;
import static org.nrg.dcm.Attributes.AcquisitionDateTime;
import static org.nrg.dcm.Attributes.AcquisitionTime;

import java.util.Arrays;
import java.util.Map;

import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.dcm.DicomAttributeIndex;

import com.google.common.base.Strings;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class AcquisitionTimeAttribute extends AbstractDateTimeAttribute implements XnatAttrDef {
    /**
     * Extracts the Date representation of DICOM Acquisition Date Time (or
     * Acquisition Date and Acquisition Time, if the DT attribute is absent)
     * and folds it into the existing accumulator.
     * @param attrName external attribute name
     * @param a fold accumulator
     * @param m map of DICOM attribute values
     * @return updated accumulator
     * @throws NoUniqueValueException if extracted value conflicts with accumulator
     */
    public static java.util.Date foldAcquisitionDateTime(String attrName,
            java.util.Date a, Map<? extends DicomAttributeIndex, ? extends String> m)
            throws NoUniqueValueException {
        final String datetime = m.get(AcquisitionDateTime);
        if (!Strings.isNullOrEmpty(datetime)) {
            return resolve(attrName, a, parseDateTime(datetime));
        }
        final String date = m.get(AcquisitionDate);
        if (Strings.isNullOrEmpty(date)) {
            return null;
        }
        final String time = m.get(AcquisitionTime);
        if (Strings.isNullOrEmpty(time)) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(date).append(time);
        return resolve(attrName, a, parseDateTime(sb.toString()));
    }

    /*
     * If there is a conflict between the accumulator a and the new value d,
     * throw a NoUniqueValueException; otherwise return the unconflicted value.
     */
    private static java.util.Date resolve(final String attrName,
            final java.util.Date a, final java.util.Date d)
    throws NoUniqueValueException {
        if (null == a) {
            return d;
        } else if (null == d || a.equals(d)) {
            return a;
        } else {
            throw new NoUniqueValueException(attrName, Arrays.asList(a, d));
        }
    }
    
    public AcquisitionTimeAttribute(final String name) {
        super(name, AcquisitionDate, AcquisitionTime, AcquisitionDateTime);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#apply(java.lang.Object)
     */
    public Iterable<ExtAttrValue> apply(final java.util.Date a) throws ExtAttrException {
        return applyTime(a);
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#foldl(java.lang.Object, java.util.Map)
     */
    public java.util.Date foldl(java.util.Date a,
            Map<? extends DicomAttributeIndex, ? extends String> m)
            throws NoUniqueValueException {
        return foldAcquisitionDateTime(getName(), a, m);
    }
}
