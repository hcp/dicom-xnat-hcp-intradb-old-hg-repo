/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.MRScanAttributes
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.hcp.dcm.DeltaTEAttr;
import org.hcp.dcm.FmriExternalInfoAttr;
import org.hcp.dcm.HCPPrivateAttributes;
import org.hcp.dcm.PMCAttr;

import org.nrg.attr.MaximumValueAttrDef;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.dcm.SiemensPhoenixProtocolAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef.Optional;

import static org.hcp.dcm.HCPPrivateAttributes.*;
import static org.nrg.dcm.DicomAttributes.*;
import static org.nrg.dcm.SiemensPrivateAttributes.*;

/**
 * mrScanData attributes
 * @author Kevin A. Archie (karchie@wustl.edu)
 */
class MRScanAttributes {
    private MRScanAttributes() {} // no instantiation

    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageScanAttributes.get());

    static {
        s.add(new VoxelResAttribute("parameters/voxelRes"));
        s.add(new OrientationAttribute("parameters/orientation"));
        s.add("parameters/subjectPosition", Tag.PatientPosition);
        s.add(HCPPrivateAttributes.receiverCoilAttribute("coil"));
        s.add(new MagneticFieldStrengthAttribute());
        s.add(new XnatAttrDef.Real("parameters/tr", MR_REPETITION_TIME));
        s.add(new MREchoTimeAttribute());
        s.add(new DeltaTEAttr());
        s.add(Optional.wrap(new XnatAttrDef.Real("parameters/ti", MR_INVERSION_TIME)));
        s.add(new XnatAttrDef.Int("parameters/flip", MR_FLIP_ANGLE));
        s.add("parameters/sequence", Tag.SequenceName);
        s.add("parameters/imageType", Tag.ImageType);
        s.add("parameters/scanSequence", Tag.ScanningSequence);
        s.add("parameters/seqVariant", Tag.SequenceVariant);
        s.add("parameters/scanOptions", Tag.ScanOptions);
        s.add("parameters/acqType", Tag.MRAcquisitionType);
        s.add(new XnatAttrDef.Real("parameters/pixelBandwidth", MR_PIXEL_BANDWIDTH));
        s.add(new ImageFOVAttribute("parameters/fov"));
        s.add(new MREchoSpacingAttribute());
        s.add("parameters/readoutSampleSpacing", SIEMENS_READOUT_SAMPLE_SPACING);

        s.add("parameters/inPlanePhaseEncoding/direction", Tag.InPlanePhaseEncodingDirection);
        s.add("parameters/inPlanePhaseEncoding/directionPositive", SIEMENS_PHASE_ENCODING_DIRECTION_POSITIVE);
        s.add("parameters/inPlanePhaseEncoding/rotation", SIEMENS_IN_PLANE_ROT);
        // Remove per Mike Harms (2016/11/02) - no longer relevant
        //s.add("parameters/inPlanePhaseEncoding/polaritySwap", HCP_EJA_POLARITY_SWAP);
        
        // Remove per Mike Harms (2016/11/02) - no longer relevant
        //s.add(new PMCAttr());
 
        s.add("parameters/diffusion/bValues", MR_DIFF_B_VALUES);
        s.add(MaximumValueAttrDef.wrap(new XnatAttrDef.Real("parameters/diffusion/bMax", SIEMENS_DIFF_B_VALUE)));
        s.add("parameters/diffusion/directionality", MR_DIFF_DIRECTION);
        s.add("parameters/diffusion/orientations", MR_DIFF_ORIENTATION);
        s.add("parameters/diffusion/anisotropyType", MR_DIFF_ANISOTROPY_TYPE);
        s.add("parameters/diffusion/refocusFlipAngle", SIEMENS_DIFF_REFOCUS_FLIP_ANGLE);
        
        s.add(new XnatAttrDef.Real("parameters/readoutSampleSpacing", 0x00191018).setScale(1e-9));
        
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_ORIENTATION_TEXT));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_TABLE_POSITION));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_GRADSPEC_LOFFSET));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_GRADSPEC_LOFFSET_VE11));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_GRADSPEC_ALSHIMCURRENT));
        
        // Remove per Mike Harms (2016/11/02) - no longer relevant
        //s.add(XnatAttrDef.AddParam.wrap(HCP_MB_RECON_LOCATION));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_MOSAIC_SLICE_COUNT));
        
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_iPAT_FACTOR));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_iPAT_N_REF_LINES));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_TX_REF_AMP));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_T_MEASURED_BASELINE_STRING));
        s.add(XnatAttrDef.AddParam.wrap(SIEMENS_COIL_STRING));
        
        // Add all alFree/adFree parameters per Mike Harms 2016/11/03.
        for (int i=0; i<100; i++) {
        	s.add(XnatAttrDef.AddParam.wrap(new SiemensPhoenixProtocolAttributeIndex.SWipMemBlock(i)));
        }
        for (int i=0; i<100; i++) {
        	s.add(XnatAttrDef.AddParam.wrap(new SiemensPhoenixProtocolAttributeIndex.SWipMemBlockAd(i)));
        }
        
        s.addAll(XnatAttrDef.AddParam.wrapAll(SiemensPhoenixProtocolAttributeIndex.FOV_POSITION));
        s.addAll(XnatAttrDef.AddParam.wrapAll(SiemensPhoenixProtocolAttributeIndex.ADJUST_VOLUME_POSITION));
        s.addAll(XnatAttrDef.AddParam.wrapAll(SiemensPhoenixProtocolAttributeIndex.NAVIGATOR_POSITION));
        
        s.add(new FmriExternalInfoAttr());
    }
}
