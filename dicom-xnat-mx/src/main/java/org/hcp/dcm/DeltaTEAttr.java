/**
 * Copyright (c) 2012 Washington University
 */
package org.hcp.dcm;

import static org.nrg.dcm.Attributes.EchoTime;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;

/**
 * Attribute that displays the (nonnegative) difference between the two in-context TEs,
 * if there are exactly two. Intended for HCP field map magnitude series.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DeltaTEAttr extends Abstract<Set<String>> implements XnatAttrDef {
    public DeltaTEAttr(final String name) {
        super(name, EchoTime);
    }

    public DeltaTEAttr() {
        this("parameters/deltaTE");
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#apply(java.lang.Object)
     */
    public Iterable<ExtAttrValue> apply(final Set<String> a) throws ExtAttrException {
        if (2 == a.size()) {
            final Iterator<String> di = a.iterator();
            try {
                if (di.hasNext()) {
                    final double d1 = Double.valueOf(di.next());
                    if (di.hasNext()) {
                        final double d2 = Double.valueOf(di.next());
                        return Collections.<ExtAttrValue>singletonList(new BasicExtAttrValue(getName(),
                                String.valueOf(Math.abs(d1-d2))));
                    }
                }
            } catch (NumberFormatException e) {
                throw new ConversionFailureException(this, a, "unable to convert", e);              
            }
        }
        return Collections.emptyList();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#foldl(java.lang.Object, java.util.Map)
     */
    public Set<String> foldl(Set<String> a, Map<? extends DicomAttributeIndex,? extends String> m) throws ExtAttrException {
        final String tes = m.get(EchoTime);
        if (!Strings.isNullOrEmpty(tes)) {
            a.add(tes);
        }
        return a;
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.Foldable#start()
     */
    public Set<String> start() { return Sets.newLinkedHashSet(); }
}
