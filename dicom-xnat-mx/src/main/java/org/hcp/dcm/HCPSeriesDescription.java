/**
 * Copyright 2012 Washington University
 */
package org.hcp.dcm;

import static org.nrg.dcm.Attributes.EchoNumbers;
import static org.nrg.dcm.Attributes.SeriesDescription;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.EvaluableAttrDef;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;

/**
 * For HCP, series_description sometimes gets filled with things other
 * than the DICOM Series Description.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class HCPSeriesDescription extends Abstract<SetMultimap<DicomAttributeIndex,String>> implements XnatAttrDef {
    public static final String RR_LABEL_REMOVED = "RR label removed";
    public static final String PMC_LABEL_REMOVED = "PMC label removed";
    
    private static final Pattern FIELDMAP_PATTERN = Pattern.compile("[^_]*fieldmap[^_]*", Pattern.CASE_INSENSITIVE);
    private static final Pattern RR_PATTERN = Pattern.compile("(.*)_RR(_SBRef)?");
    private static final Pattern PMC_PATTERN = Pattern.compile("(.*)_PMC");
    
    private static final XnatAttrDef.Constant rrLabelRemoved = new XnatAttrDef.Constant(RR_LABEL_REMOVED, Boolean.TRUE.toString());
    private static final XnatAttrDef.Constant pmcLabelRemoved = new XnatAttrDef.Constant(PMC_LABEL_REMOVED, Boolean.TRUE.toString());

    private final Logger logger = LoggerFactory.getLogger(HCPSeriesDescription.class);
    
    public HCPSeriesDescription() {
        super("series_description", SeriesDescription, EchoNumbers);
    }

    private final String applyPattern(final String in, final Pattern pattern,
            final ImmutableList.Builder<ExtAttrValue> builder, final EvaluableAttrDef<DicomAttributeIndex,?,?>...indicators) {
        final Matcher matcher = pattern.matcher(in);
        if (matcher.matches()) {
            final StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= matcher.groupCount(); i++) {
                final String g = matcher.group(i);
                if (!Strings.isNullOrEmpty(g)) {
                    sb.append(g);
                }
            }
            for (final EvaluableAttrDef<DicomAttributeIndex,?,?> indicator : indicators) {
                try {
                    builder.addAll(AddParam.wrap(indicator).apply(null));
                } catch (ExtAttrException e) {
                    logger.error("failed to apply indicator attribute " + indicator, e);
                }
            }
            return sb.toString();
        } else {
            return in;
        }
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#apply(java.lang.Object)
     */
    public Iterable<ExtAttrValue> apply(SetMultimap<DicomAttributeIndex,String> a) throws ExtAttrException {
        String desc;
        try {
            desc = Iterables.getOnlyElement(a.get(SeriesDescription));
            if (Strings.isNullOrEmpty(desc)) {
                return Collections.emptyList();
            }
        } catch (Throwable t) {
            return Collections.emptyList();
        }

        if (FIELDMAP_PATTERN.matcher(desc).matches()) {
            final Set<String> echons = a.get(EchoNumbers);
            if (null == echons) {
                return applyString(desc);
            } else if (1 == echons.size()) {
                return applyString(desc + "_Phase");
            } else if (2 == echons.size()) {
                return applyString(desc + "_Magnitude");
            } else {
                return applyString(desc);
            }
        }

        final ImmutableList.Builder<ExtAttrValue> vsb = ImmutableList.builder();
        desc = applyPattern(desc, RR_PATTERN, vsb, rrLabelRemoved);
        desc = applyPattern(desc, PMC_PATTERN, vsb, pmcLabelRemoved);
        
        vsb.add(new BasicExtAttrValue(getName(), desc));
        return vsb.build();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#foldl(java.lang.Object, java.util.Map)
     */
    public SetMultimap<DicomAttributeIndex,String>
    foldl(SetMultimap<DicomAttributeIndex,String> a, Map<? extends DicomAttributeIndex, ? extends String> m) {
        final String desc = m.get(SeriesDescription);
        if (!Strings.isNullOrEmpty(desc)) {
            a.put(SeriesDescription, desc);
        }
        final String echon = m.get(EchoNumbers);
        if (!Strings.isNullOrEmpty(desc)) {
            a.put(EchoNumbers, echon);
        }
        return a;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.Foldable#start()
     */
    public SetMultimap<DicomAttributeIndex,String> start() {
        return LinkedHashMultimap.create();
    }
}
