/**
 * Copyright 2012 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.nrg.attr.EvaluableAttrDef;
import org.nrg.attr.ExtAttrException;
import org.nrg.dcm.DicomAttributeIndex;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

import static org.nrg.dcm.Attributes.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ScanStartTimeAttrDefTest {

    /**
     * Test method for {@link org.nrg.dcm.xnat.ScanStartTimeAttrDef#apply(java.util.Date)}.
     */
    @Test
    public void testApply() throws ExtAttrException {
        final EvaluableAttrDef<?,?,Date> def = new ScanStartTimeAttrDef();
        final Date d = Calendar.getInstance().getTime();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");
        assertEquals(df.format(d), Iterables.getOnlyElement(def.apply(d)).getText());
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.ScanStartTimeAttrDef#foldl(java.util.Date, java.util.Map)}.
     */
    @Test
    public void testFoldlOne() throws ExtAttrException {
        final EvaluableAttrDef<DicomAttributeIndex,String,Date> def = new ScanStartTimeAttrDef();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");

        Date a = def.start();
        assertNull(a);
        a = def.foldl(a, ImmutableMap.of(SeriesDate, "20120901", SeriesTime, "103035"));
        assertEquals("10:30:35", df.format(a));
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.ScanStartTimeAttrDef#foldl(java.util.Date, java.util.Map)}.
     */
    @Test
    public void testFoldlOneWithAcqTime() throws ExtAttrException {
        final EvaluableAttrDef<DicomAttributeIndex,String,Date> def = new ScanStartTimeAttrDef();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");

        Date a = def.start();
        assertNull(a);
        a = def.foldl(a, ImmutableMap.of(SeriesDate, "20120901", SeriesTime, "103035",
                AcquisitionDate, "20120901", AcquisitionTime, "100510"));
        assertEquals("10:05:10", df.format(a));
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.ScanStartTimeAttrDef#foldl(java.util.Date, java.util.Map)}.
     */
    @Test
    public void testFoldlOneWithAcqDateTime() throws ExtAttrException {
        final EvaluableAttrDef<DicomAttributeIndex,String,Date> def = new ScanStartTimeAttrDef();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");

        Date a = def.start();
        assertNull(a);
        a = def.foldl(a, ImmutableMap.of(SeriesDate, "20120901", SeriesTime, "103035",
                AcquisitionDateTime, "20120901100508"));
        assertEquals("10:05:08", df.format(a));
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.ScanStartTimeAttrDef#foldl(java.util.Date, java.util.Map)}.
     */
    @Test
    public void testFoldlMult() throws ExtAttrException {
        final EvaluableAttrDef<DicomAttributeIndex,String,Date> def = new ScanStartTimeAttrDef();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");

        Date a = def.start();
        assertNull(a);
        a = def.foldl(a, ImmutableMap.of(SeriesDate, "20120901", SeriesTime, "103035",
                AcquisitionDate, "20120901", AcquisitionTime, "100510"));
        assertEquals("10:05:10", df.format(a));
        a = def.foldl(a, ImmutableMap.of(SeriesDate, "20120901", SeriesTime, "103035",
                AcquisitionDate, "20120901", AcquisitionTime, "100508"));
        assertEquals("10:05:08", df.format(a));
        a = def.foldl(a, ImmutableMap.of(SeriesDate, "20120901", SeriesTime, "103035",
                AcquisitionDate, "20120901", AcquisitionTime, "100512"));
        assertEquals("10:05:08", df.format(a));
    }
}
